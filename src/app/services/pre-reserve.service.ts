import {Injectable} from '@angular/core';
import {SourceComponent} from '../source/source.component';

@Injectable({providedIn: 'root'})

export class PreReserveService {

  arrayPreReserve: Array<any> = [];
  firstIndex: number;
  cutedText: string;
  secondIndex: number;
  arrayElement: string;
  trimedArrayElement: string;

  constructor(private sourceComponent: SourceComponent) {
  }

  getPreReserve() {
    this.firstIndex = this.sourceComponent.source.indexOf('PreReserve');
    if (this.firstIndex === -1) {
      return console.log('finish');
    }
    this.cutedText = this.sourceComponent.source.slice(this.firstIndex + 10);
    this.secondIndex = this.firstIndex + 1 + this.cutedText.indexOf('PreReserve');
    this.arrayElement = this.sourceComponent.source.substring(this.firstIndex, this.secondIndex);
    this.trimedArrayElement = this.arrayElement.trim();
    this.arrayPreReserve.push(this.trimedArrayElement);
    this.sourceComponent.source = this.sourceComponent.source.slice(this.secondIndex + 10);
    this.getPreReserve();
  }
}
