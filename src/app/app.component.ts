import {Component, Renderer2, ElementRef, ViewChild, AfterViewInit, ViewEncapsulation} from '@angular/core';
import {PreReserveService} from './services/pre-reserve.service';

declare var $: any;

// Task
// Create table only from "PreReserve" rows

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements AfterViewInit {

  title = 'Data table for PreReserve rows';
  regexpAll = RegExp(/\bPreReserve\b:(\s+[-0-9a-zA-Z_.]+\s+[=]\s+[-0-9a-zA-Z_.]+[,]?){6}/, 'g');
  regexpRemoveValue = RegExp(/\bPreReserve\b:\s+|[=]\s+[-0-9a-zA-Z_.]+[,]\s+|[=]\s+[-0-9a-zA-Z_.]+/, 'g');
  regexpRemoveProperty = RegExp(/\bPreReserve\b:|\s+[-0-9a-zA-Z_.]+\s+=\s+/, 'g');
  PreReserve: Array<any> = [];
  objectProperty: string;
  objectValue: string;

  @ViewChild('unRegexTable') private unRegexTable: ElementRef;

  constructor(private renderer: Renderer2, private getPreReserveService: PreReserveService) {
    getPreReserveService.getPreReserve();
    console.log(this.getPreReserveService.arrayPreReserve);
  }

  ngAfterViewInit() {

    const headNames = ['cnnId', 'account_id', 'full_reserve', 'cur_cnn_reserve', 'cnn_count', 'balance'];
    const mainTable = $('<table>').attr({id: 'mainTable', border: '1px'});
    const errorTable = $('<table>').attr({id: 'errorTable', border: '1px'});

    const mainTableHeader = $('<tr>');
    const errorTableHeader = $('<tr>');

    // tslint:disable-next-line:forin
    for (const name in headNames) {
      mainTableHeader.append($('<td>').append($('<b>').text(headNames[name])));
      errorTableHeader.append($('<td>').append($('<b>').text(headNames[name])));
    }

    const mainTableHeaderText = $('<p>').attr({class: 'tableName'}).text('Main table');
    const errorTableHeaderText = $('<p>').attr({class: 'tableName'}).text('Error table');

    $(mainTable).append(mainTableHeader);
    $(errorTable).append(errorTableHeader);
    const appRoot = document.body.getElementsByTagName('app-root')[0];
    this.renderer.appendChild(appRoot, mainTableHeaderText[0]);
    this.renderer.appendChild(appRoot, mainTable[0]);
    this.renderer.appendChild(appRoot, errorTableHeaderText[0]);
    this.renderer.appendChild(appRoot, errorTable[0]);

    for (const z of this.getPreReserveService.arrayPreReserve) {

      if (z.search(this.regexpAll)) {
        this.unRegex(z);
      }

      while ((this.regexpAll.exec(z)) !== null) {
        this.objectProperty = z;
        this.objectValue = this.objectProperty;

        let arrayProperty = [];
        arrayProperty.push(this.objectProperty.replace(this.regexpRemoveValue, '').split(' '));
        arrayProperty = arrayProperty[0];
        arrayProperty.pop();

        let arrayValue = [];
        arrayValue.push(this.objectValue.replace(this.regexpRemoveProperty, '').split(','));
        arrayValue = arrayValue[0];

        const tmpObj = {};

        tmpObj[arrayProperty[0]] = arrayValue[0];
        tmpObj[arrayProperty[1]] = arrayValue[1];
        tmpObj[arrayProperty[2]] = arrayValue[2];
        tmpObj[arrayProperty[3]] = arrayValue[3];
        tmpObj[arrayProperty[4]] = arrayValue[4];
        tmpObj[arrayProperty[5]] = arrayValue[5];

        this.PreReserve.push(tmpObj);
      }
    }
    console.log(this.PreReserve);

    let countMainTable = 1;

    // tslint:disable-next-line:forin
    for (const i in this.PreReserve) {

      const propertyObj = Object.getOwnPropertyNames(this.PreReserve[i]);

      switch (true) {
        case isNaN(this.PreReserve[i][propertyObj[0]]):
          this.createErrorTable(this.PreReserve[i][propertyObj[0]], i);
          console.log(this.PreReserve[i][propertyObj[0]] + ' isn"t number');
          break;
        case isNaN(this.PreReserve[i][propertyObj[1]]):
          this.createErrorTable(this.PreReserve[i][propertyObj[1]], i);
          console.log(this.PreReserve[i][propertyObj[1]] + ' isn"t number');
          break;
        case isNaN(this.PreReserve[i][propertyObj[2]]):
          this.createErrorTable(this.PreReserve[i][propertyObj[2]], i);
          console.log(this.PreReserve[i][propertyObj[2]] + ' isn"t number');
          break;
        case isNaN(this.PreReserve[i][propertyObj[3]]):
          this.createErrorTable(this.PreReserve[i][propertyObj[3]], i);
          console.log(this.PreReserve[i][propertyObj[3]] + ' isn"t number');
          break;
        case isNaN(this.PreReserve[i][propertyObj[4]]):
          this.createErrorTable(this.PreReserve[i][propertyObj[4]], i);
          console.log(this.PreReserve[i][propertyObj[4]] + ' isn"t number');
          break;
        case isNaN(this.PreReserve[i][propertyObj[5]]):
          this.createErrorTable(this.PreReserve[i][propertyObj[5]], i);
          console.log(this.PreReserve[i][propertyObj[5]] + ' isn"t number');
          break;
        default:
          const mainTableRow = $('<tr>');
          $('#mainTable').append(mainTableRow);

          mainTableRow.append($('<td>').attr({id: propertyObj[0] + '_' + countMainTable}).append($('<p>').text(this.PreReserve[i][propertyObj[0]])));
          mainTableRow.append($('<td>').attr({id: propertyObj[1] + '_' + countMainTable}).append($('<p>').text(this.PreReserve[i][propertyObj[1]])));
          mainTableRow.append($('<td>').attr({id: propertyObj[2] + '_' + countMainTable}).append($('<p>').text(this.PreReserve[i][propertyObj[2]])));
          mainTableRow.append($('<td>').attr({id: propertyObj[3] + '_' + countMainTable}).append($('<p>').text(this.PreReserve[i][propertyObj[3]])));
          mainTableRow.append($('<td>').attr({id: propertyObj[4] + '_' + countMainTable}).append($('<p>').text(this.PreReserve[i][propertyObj[4]])));
          mainTableRow.append($('<td>').attr({id: propertyObj[5] + '_' + countMainTable}).append($('<p>').text(this.PreReserve[i][propertyObj[5]])));

          try {
            if (propertyObj[0] !== 'cnnId') {
              throw propertyObj[0];
            }
            if (propertyObj[1] !== 'account_id') {
              throw propertyObj[1];
            }
            if (propertyObj[2] !== 'full_reserve') {
              throw propertyObj[2];
            }
            if (propertyObj[3] !== 'cur_cnn_reserve') {
              throw propertyObj[3];
            }
            if (propertyObj[4] !== 'cnn_count') {
              throw propertyObj[4];
            }
            if (propertyObj[5] !== 'balance') {
              throw propertyObj[5];
            }
          } catch (error) {
            $('#' + error + '_' + countMainTable).css({backgroundColor: 'yellow'}).attr({title: 'Wrong property name: ' + error}).tooltip();
            console.log(error + ' isn"t true');
          } finally {
            countMainTable++;
          }
      }
    }
  }

  createErrorTable(error: string, i: string) {
    let countErrorTable = 1;

    const errorTableRow = $('<tr>');
    $('#errorTable').append(errorTableRow);

    const propertyObj = Object.getOwnPropertyNames(this.PreReserve[i]);

    errorTableRow.append($('<td>').attr({id: propertyObj[0] + '_' + countErrorTable}).append($('<p>').text(this.PreReserve[i][propertyObj[0]])));
    errorTableRow.append($('<td>').attr({id: propertyObj[1] + '_' + countErrorTable}).append($('<p>').text(this.PreReserve[i][propertyObj[1]])));
    errorTableRow.append($('<td>').attr({id: propertyObj[2] + '_' + countErrorTable}).append($('<p>').text(this.PreReserve[i][propertyObj[2]])));
    errorTableRow.append($('<td>').attr({id: propertyObj[3] + '_' + countErrorTable}).append($('<p>').text(this.PreReserve[i][propertyObj[3]])));
    errorTableRow.append($('<td>').attr({id: propertyObj[4] + '_' + countErrorTable}).append($('<p>').text(this.PreReserve[i][propertyObj[4]])));
    errorTableRow.append($('<td>').attr({id: propertyObj[5] + '_' + countErrorTable}).append($('<p>').text(this.PreReserve[i][propertyObj[5]])));

    $('td:contains(' + error + ')').css({backgroundColor: 'red'}).attr({title: 'Data is NaN'}).tooltip();

    try {
      if (propertyObj[0] !== 'cnnId') {
        throw propertyObj[0];
      }
      if (propertyObj[1] !== 'account_id') {
        throw propertyObj[1];
      }
      if (propertyObj[2] !== 'full_reserve') {
        throw propertyObj[2];
      }
      if (propertyObj[3] !== 'cur_cnn_reserve') {
        throw propertyObj[3];
      }
      if (propertyObj[4] !== 'cnn_count') {
        throw propertyObj[4];
      }
      if (propertyObj[5] !== 'balance') {
        throw propertyObj[5];
      }
    } catch (error) {

      $('#' + error + '_' + countErrorTable).css({backgroundColor: 'yellow'}).attr({title: 'Wrong property name: ' + error}).tooltip();
      console.log(error + ' isn"t true');
    } finally {
      countErrorTable++;
    }
  }

  unRegex(unRegexRow: string) {
    const unRegexTableRow = $('<tr>').append($('<td>').append($('<p>').text(unRegexRow)));
    this.renderer.appendChild(this.unRegexTable.nativeElement, unRegexTableRow[0]);
    console.log(this.unRegexTable.nativeElement);

    // const unregexTableRow = this.renderer.createElement('tr');
    // const td = this.renderer.createElement('td');
    // const  p = this.renderer.createElement('p');
    // const  unregexTableRowTextNode = this.renderer.createText(unRegexRow);
    // this.renderer.appendChild(p, unregexTableRowTextNode);
    // this.renderer.appendChild(td, p);
    // this.renderer.appendChild(unregexTableRow, td);
    // this.renderer.appendChild(this.unregexTable.nativeElement, unregexTableRow);
    // console.log(this.unregexTable.nativeElement);
  }
}


